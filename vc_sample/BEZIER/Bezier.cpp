// Bezier.cpp: implementation of the CBezier class.
//http://fuliang.javaeye.com/blog/69307
//http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/Bezier/de-casteljau.html
//http://courseware2.itsinghua.com/course/yjs/jsj/txx/4-2/zx/txx/txx_3_2.htm
//////////////////////////////////////////////////////////////////////

#include "Bezier.h"

#define THETA 0.001

//两失量(x1,y1)和(x2,y2)的夹角的cosA,sinA值
void CalcCosSin(double x1,double y1,double x2,double y2,
				double *cosA,double *sinA)
{  
	//初值为0度角，即不作任何旋转
	*cosA=1; //cos(0)=1
	*sinA=0; //sin(0)=0

	//求两失量夹角的cos值
	double c=sqrt(x1*x1+y1*y1)*sqrt(x2*x2+y2*y2);
	if(c==0) return;
	c = (x1*x2+y1*y2)/c;
	*cosA=c;

	//下面判断是逆时针方向旋转，还是顺时针方向旋转
	//失量 A(x1,y1,0)与B(x2,y2,0)的叉积为：B(0,0,x1*y2-y1*x2)
	//如果x1*y2-y1*x2<0，表示B失量指向Z轴负方向，表示A顺时针转到B，sinA应该<0
	//反之，sinA应该>0
	if(x1*y2-y1*x2<0)
		*sinA=-sqrt(1-c*c);//如果是顺时针方向旋转 sinA<0
	else
		*sinA=sqrt(1-c*c);  //如果是逆时针方向旋转 sinA>0
}
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
/*
	矩阵乘法
	Matrix aa * bb -----  (l*m)*(m*n)=>(l*n) 
*/
void matrix(double aa[][3],double bb[][3],int l,int m,int n)
{
	register double cc[7][3];//7个点
	register int i,j,k;
    for(i=0;i<l;i++) {
            for(j=0;j<n;j++){
                    cc[i][j]=0;
                    for(k=0;k<m;k++) 
						cc[i][j]=cc[i][j]+aa[i][k]*bb[k][j];
           }
    }
    for(i=0;i<l;i++)
	for(j=0;j<n;j++) aa[i][j]=cc[i][j];
}

CBezier::CBezier()
{
	m_nPoint=0;
	m_Point[0].x=310;m_Point[0].y=30;
	m_Point[1].x=230;m_Point[1].y=120;
	m_Point[2].x=270;m_Point[2].y=400;
	m_Point[3].x=323;m_Point[3].y=430;
	
	m_Point[4].x=520;m_Point[4].y=400;
	m_Point[5].x=510;m_Point[5].y=130;
	m_Point[6].x=430;m_Point[6].y=30;
	m_nPoint=7;
	int i;
	for(i=0;i<7;i++)
	{
		p[i][0]=m_Point[i].x;
		p[i][1]=m_Point[i].y;
		p[i][2]=1;//齐次坐标的1
	}
	CalcCenter();
}

//计算形体的中心点，便于旋转
void CBezier::CalcCenter()
{
	int minX,minY,maxX,maxY;
	int i;
	minX=m_Point[0].x;
	minY=m_Point[0].y;
	maxX=minX;
	maxY=minY;
	for(i=1;i<7;i++)
	{
		if(minX>m_Point[i].x)
			minX=m_Point[i].x;
		if(minY>m_Point[i].y)
			minY=m_Point[i].y;

		if(maxX<m_Point[i].x)
			maxX=m_Point[i].x;
		if(maxY<m_Point[i].y)
			maxY=m_Point[i].y;		
	}
	m_Center.x=(minX+maxX)/2;
	m_Center.y=(minY+maxY)/2;

}
CBezier::~CBezier()
{

}
void CBezier::Draw(CDC *dc)
{
	int i,j,nPoint;
	double tt;
	double step;
	CPen dotpen(PS_DOT,1,RGB(0,0,200)),*oldpen;
	CPen pen(PS_SOLID,1,RGB(0,0,0));
	CPoint *point=m_Point;
	CPoint p[BEZIER_STEP+1];

	step=(double)1.0/(double)BEZIER_STEP;
	nPoint=m_nPoint;

	oldpen=(CPen*)dc->SelectObject(&pen);
	while(nPoint>=4)
	{
		for(tt=0.0,j=0;tt<1.01;tt=tt+step,j++)
		{
			//以下两种方法二选一
			p[j]=hor(3,point,tt);
			//p[j]=decas(3,point,tt);
		}
		dc->SelectObject(&dotpen);
		dc->MoveTo(point[0].x,point[0].y);
		for(i=1;i<4;i++)
		{
			dc->LineTo(point[i].x,point[i].y);
		}
		dc->SelectObject(pen);
		dc->MoveTo(p[0].x,p[0].y);
		for(i=1;i<j;i++)
		{		
			dc->LineTo(p[i].x,p[i].y);
		}
		nPoint-=3;
		point+=3;
	}
	//画中心点
	dc->Ellipse(m_Center.x-5,m_Center.y-5,m_Center.x+5,m_Center.y+5);
	dc->SelectObject(oldpen);
}

CPoint CBezier::hor(int degree, CPoint *point, double t)
{
	int i,choose_i;
	double fact,t1,x,y;
	CPoint aux;
	t1=1.0-t;
	fact=1.0;
	choose_i=1;
	x=(double)point[0].x*t1;
	y=(double)point[0].y*t1;
	for(i=1;i<degree;i++)
	{
		fact=fact*t;
		choose_i=choose_i*(degree-i+1)/i;
		x=(x+fact*choose_i*point[i].x)*t1;
		y=(y+fact*choose_i*point[i].y)*t1;
	}
	x=x+fact*t*(double)point[degree].x;
	y=y+fact*t*(double)point[degree].y;
	aux.x=(long)x;
	aux.y=(long)y;
	return aux;
}
//********************************
//绘制Bezier曲线的辅助函数，
//主要完成t在某个值时，bezier
//曲线上的一个点的坐标的计算
//*********************************
CPoint CBezier::decas(int degree, CPoint *point, double t)
{
	int r,i;
	POINT coeffa[10];//用POINTF是为了保证精度
	double t1;
	t1=1.0-t;
	for(i=0;i<=degree;i++)	
	{
		coeffa[i].x=(float)point[i].x;
		coeffa[i].y=(float)point[i].y;
	}
	for(r=1;r<=degree;r++)
		for(i=0;i<=degree-r;i++)
		{
			coeffa[i].x=(long)(t1*coeffa[i].x+t*coeffa[i+1].x);
			coeffa[i].y=(long)(t1*coeffa[i].y+t*coeffa[i+1].y);
		}
	POINT p;
	p.x=(int)coeffa[0].x;
	p.y=(int)coeffa[0].y;
	return p;
}

//点是否击中Bezier曲线
BOOL CBezier::PtInBezier(CPoint point)
{
	int i,j,nPoint;
	double tt;
	double step;
	CRect rec;
	int RecWidth=5;

	CPoint *pnt=m_Point;	
	CPoint p[BEZIER_STEP+1];

	step=(double)1.0/(double)BEZIER_STEP;
	nPoint=m_nPoint;

	while(nPoint>=4)
	{
		for(tt=0.0,j=0;tt<1.01;tt=tt+step,j++)
			p[j]=hor(3,pnt,tt);
		for(i=1;i<j;i++)
		{
			rec.left=p[i-1].x;
			rec.top=p[i-1].y;
			rec.right=p[i].x;
			rec.bottom=p[i].y;		
			rec.NormalizeRect();
			rec.InflateRect(1,1,1,1);
			if(PtInRect(&rec,point)) return TRUE;
		}
		nPoint-=3;
		pnt+=3;
	}
	return FALSE;
}

//平移
void CBezier::Move(int offx, int offy)
{
	UINT i;
	for(i=0;i<7;i++)
	{
		p[i][0]+=offx;
		p[i][1]+=offy;
		p[i][2]=1;//齐次坐标的1
	}
	for(i=0;i<7;i++)
	{
		m_Point[i].x=(long)p[i][0];
		m_Point[i].y=(long)p[i][1];	
		//p[i][2]=1;
	}
}

//设置控制点的值
void CBezier::setPoint(int index,CPoint point)
{
	m_Point[index]=point;
	p[index][0]=point.x;
	p[index][1]=point.y;
	p[index][2]=1;
}

//旋转
//由m_Center，startPoint, endPoint可以组成一个三角形
//本函数处理startPoint绕m_Center旋转到endPoint处的变换
void CBezier::Rotate(CPoint startPoint,CPoint endPoint)
{	
	double tt[3][3];
	double angle=THETA;
	int i,j;

	/* 先初始化变换矩阵为单位矩阵*/
	for (i=0;i<3;i++)  
		for(j=0;j<3;j++) 
			tt[i][j]=0;
	tt[0][0]=1;
	tt[1][1]=1;
	tt[2][2]=1;

	double cosA,sinA;

	//计算出cosA,和sinA
	CalcCosSin(startPoint.x-m_Center.x,startPoint.y-m_Center.y,
		endPoint.x-m_Center.x,endPoint.y-m_Center.y,
		&cosA,&sinA);

	//预设复合旋转的变换矩阵tt[3][3]
	//Page 369
	tt[0][0]=cosA;
	tt[0][1]=sinA;
	tt[1][0]=-sinA;
	tt[1][1]=cosA;
	tt[2][0]=(1-cosA)*m_Center.x+sinA*m_Center.y;
	tt[2][1]=(1-cosA)*m_Center.y-sinA*m_Center.x;

	//开始变换，点列矩阵p经过变换tt后，仍然是p，只是坐值改变了
    matrix(p,tt,7,3,3);
	
	//变换完成后将变换后的数据p，转换为显示用的m_Point[]
	for(i=0;i<7;i++)
	{
		m_Point[i].x=(long)p[i][0];
		m_Point[i].y=(long)p[i][1];	
		p[i][2]=1;
	}
}
#include <iostream.h>
class CBucket//定义桶类  
{
public:
	int ScanLine;//声明扫描线位置
	CBucket *pNext;//声明指向下一桶结点的指针
};
void main()
{
	CBucket *pHead=new CBucket,*pCurrentB;//定义桶结点的头指针和桶结点的当前指针
	pCurrentB=pHead;//当前指针为桶结点的头指针，开始给第一个结点赋值
	pCurrentB->ScanLine=1;//当前结点(第一个结点)的扫描线为1
	cout<<"第一个结点的扫描线位置是："//输出当前结点的扫描线置
		<<pCurrentB->ScanLine<<endl;
	pCurrentB->pNext=new CBucket;//当前结点(第一个结点)的pNext指针指向新建结点
	pCurrentB=pCurrentB->pNext;//当前指针指向新建结点
	pCurrentB->ScanLine=2;//当前结点(第二个结点)的扫描线位置为2
	cout<<"第二个结点的扫描线位置是："//输出当前结点的扫描线位置
		<<pCurrentB->ScanLine<<endl;
	pCurrentB->pNext=NULL;//只构造两个结点，当前结点(第二个结点)的pNext指针置空
	delete pHead;//释放头结点
	delete pCurrentB;//释放当前结点
	pHead=NULL;//释放头指针
	pCurrentB=NULL;//释放当前指针
}

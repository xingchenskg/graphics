// TestView.cpp : implementation of the CTestView class
//

#include "stdafx.h"
#include "Test.h"

#include "TestDoc.h"
#include "TestView.h"
#define ROUND(a) int(a+0.5)
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTestView

IMPLEMENT_DYNCREATE(CTestView, CView)

BEGIN_MESSAGE_MAP(CTestView, CView)
	//{{AFX_MSG_MAP(CTestView)
	ON_COMMAND(ID_MSTYLE, OnMstyle)
	ON_COMMAND(ID_MLAY, OnMlay)
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTestView construction/destruction

CTestView::CTestView()
{
	// TODO: add construction code here
	Edge=180;
	P=new CP2[31];//中心点;
}

CTestView::~CTestView()
{
}

BOOL CTestView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CTestView drawing

void CTestView::OnDraw(CDC* pDC)
{
	CTestDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here	
}

/////////////////////////////////////////////////////////////////////////////
// CTestView printing

BOOL CTestView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CTestView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CTestView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CTestView diagnostics

#ifdef _DEBUG
void CTestView::AssertValid() const
{
	CView::AssertValid();
}

void CTestView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CTestDoc* CTestView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CTestDoc)));
	return (CTestDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTestView message handlers

void CTestView::DrawObject()//绘制地砖
{
	CRect Rect;
	GetClientRect(&Rect);
	CDC *pDC=GetDC();
	pDC->SetMapMode(MM_ANISOTROPIC);//自定义坐标系
	pDC->SetWindowExt(Rect.Width(),Rect.Height());
	pDC->SetViewportExt(Rect.Width(),-Rect.Height());//x轴水平向右，y轴垂直向上
	pDC->SetViewportOrg(Rect.Width()/2,Rect.Height()/2);//屏幕中心为原点
	CPen NewPen,*pOldPen; 
	NewPen.CreatePen(PS_SOLID,3,RGB(208,102,50)); 
	pOldPen=pDC->SelectObject(&NewPen);
	CBrush NewBrush,*pOldBrush;
	NewBrush.CreateSolidBrush(RGB(231,127,76));
	pOldBrush=pDC->SelectObject(&NewBrush);
	pDC->Rectangle(ROUND(P[1].x),ROUND(P[1].y),ROUND(P[16].x),ROUND(P[16].y));

	pDC->Rectangle(ROUND(P[1].x),ROUND(P[1].y),ROUND(P[6].x),ROUND(P[6].y));
	pDC->Rectangle(ROUND(P[2].x),ROUND(P[2].y),ROUND(P[7].x),ROUND(P[7].y));
	pDC->Rectangle(ROUND(P[3].x),ROUND(P[3].y),ROUND(P[8].x),ROUND(P[8].y));

	pDC->Rectangle(ROUND(P[5].x),ROUND(P[5].y),ROUND(P[10].x),ROUND(P[10].y));
	pDC->Rectangle(ROUND(P[6].x),ROUND(P[6].y),ROUND(P[11].x),ROUND(P[11].y));
	pDC->Rectangle(ROUND(P[7].x),ROUND(P[7].y),ROUND(P[12].x),ROUND(P[12].y));
	
	pDC->Rectangle(ROUND(P[9].x),ROUND(P[9].y),ROUND(P[14].x),ROUND(P[14].y));
	pDC->Rectangle(ROUND(P[10].x),ROUND(P[10].y),ROUND(P[15].x),ROUND(P[15].y));
	pDC->Rectangle(ROUND(P[11].x),ROUND(P[11].y),ROUND(P[0].x),ROUND(P[0].y));

	pDC->Rectangle(ROUND(P[21].x),ROUND(P[21].y),ROUND(P[16].x),ROUND(P[16].y));
	pDC->Rectangle(ROUND(P[22].x),ROUND(P[22].y),ROUND(P[17].x),ROUND(P[17].y));
	pDC->Rectangle(ROUND(P[23].x),ROUND(P[23].y),ROUND(P[18].x),ROUND(P[18].y));

	pDC->Rectangle(ROUND(P[25].x),ROUND(P[25].y),ROUND(P[20].x),ROUND(P[20].y));
	pDC->Rectangle(ROUND(P[26].x),ROUND(P[26].y),ROUND(P[21].x),ROUND(P[21].y));
	pDC->Rectangle(ROUND(P[27].x),ROUND(P[27].y),ROUND(P[22].x),ROUND(P[22].y));
	
	pDC->Rectangle(ROUND(P[29].x),ROUND(P[29].y),ROUND(P[24].x),ROUND(P[24].y));
	pDC->Rectangle(ROUND(P[30].x),ROUND(P[30].y),ROUND(P[25].x),ROUND(P[25].y));
	pDC->Rectangle(ROUND(P[0].x),ROUND(P[0].y),ROUND(P[26].x),ROUND(P[26].y));

	pDC->Ellipse(ROUND(P[11].x),ROUND(P[11].y),ROUND(P[26].x),ROUND(P[26].y));
	pDC->Arc(ROUND(P[6].x),ROUND(P[6].y),ROUND(P[21].x),ROUND(P[21].y),ROUND(P[29].x),ROUND(P[29].y),ROUND(P[8].x),ROUND(P[8].y));
	pDC->Arc(ROUND(P[1].x),ROUND(P[1].y),ROUND(P[16].x),ROUND(P[16].y),ROUND(P[28].x),ROUND(P[28].y),ROUND(P[4].x),ROUND(P[4].y));
	pDC->Arc(ROUND(P[6].x),ROUND(P[6].y),ROUND(P[21].x),ROUND(P[21].y),ROUND(P[14].x),ROUND(P[14].y),ROUND(P[23].x),ROUND(P[23].y));
	pDC->Arc(ROUND(P[1].x),ROUND(P[1].y),ROUND(P[16].x),ROUND(P[16].y),ROUND(P[13].x),ROUND(P[13].y),ROUND(P[19].x),ROUND(P[19].y));
	pDC->SelectObject(pOldPen);
	NewPen.DeleteObject();
	pDC->SelectObject(pOldBrush);
	NewBrush.DeleteObject();
	ReleaseDC(pDC);
}


void CTestView::OnMstyle()//设计地砖样式
{
	// TODO: Add your command handler code here
	RedrawWindow();
	ReadPoint();
	DrawObject();	
}

void CTestView::ReadPoint()
{
	P[0]=CP2(0,0);//正方形中心点
	P[1]=CP2(P[0].x-Edge/2,P[0].y+Edge/2);

	P[2]=CP2(P[0].x-Edge/3,P[0].y+Edge/2);

	P[3]=CP2(P[0].x-Edge/6,P[0].y+Edge/2);

	P[4]=CP2(P[0].x,P[0].y+Edge/2);

	P[5]=CP2(P[0].x-Edge/2,P[0].y+Edge/3);

	P[6]=CP2(P[0].x-Edge/3,P[0].y+Edge/3);

	P[7]=CP2(P[0].x-Edge/6,P[0].y+Edge/3);

	P[8]=CP2(P[0].x,P[0].y+Edge/3);

	P[9]=CP2(P[0].x-Edge/2,P[0].y+Edge/6);

	P[10]=CP2(P[0].x-Edge/3,P[0].y+Edge/6);

	P[11]=CP2(P[0].x-Edge/6,P[0].y+Edge/6);

	P[12]=CP2(P[0].x,P[0].y+Edge/6);

	P[13]=CP2(P[0].x-Edge/2,P[0].y);

	P[14]=CP2(P[0].x-Edge/3,P[0].y);

	P[15]=CP2(P[0].x-Edge/6,P[0].y);

	P[16]=CP2(P[0].x+Edge/2,P[0].y-Edge/2);
	P[17]=CP2(P[0].x+Edge/3,P[0].y-Edge/2);
	P[18]=CP2(P[0].x+Edge/6,P[0].y-Edge/2);
	P[19]=CP2(P[0].x,P[0].y-Edge/2);
	
	P[20]=CP2(P[0].x+Edge/2,P[0].y-Edge/3);
	P[21]=CP2(P[0].x+Edge/3,P[0].y-Edge/3);
	P[22]=CP2(P[0].x+Edge/6,P[0].y-Edge/3);
	P[23]=CP2(P[0].x,P[0].y-Edge/3);


	P[24]=CP2(P[0].x+Edge/2,P[0].y-Edge/6);
	P[25]=CP2(P[0].x+Edge/3,P[0].y-Edge/6);
	P[26]=CP2(P[0].x+Edge/6,P[0].y-Edge/6);
	P[27]=CP2(P[0].x,P[0].y-Edge/6);

	P[28]=CP2(P[0].x+Edge/2,P[0].y);
	P[29]=CP2(P[0].x+Edge/3,P[0].y);
	P[30]=CP2(P[0].x+Edge/6,P[0].y);
}

void CTestView::OnMlay() //铺设地砖
{
	// TODO: Add your command handler code here
	tran.SetMat(P,31);
	tran.Translate(-Edge,Edge);
	DrawObject();//上左
	tran.Translate(Edge,-Edge);

	tran.Rotate(90,P[0]);
	tran.Translate(0,Edge);
	DrawObject();//上中
	tran.Translate(0,-Edge);

	tran.Rotate(90,P[0]);
	tran.Translate(Edge,Edge);
	DrawObject();//上右
	tran.Translate(-Edge,-Edge);

	tran.Rotate(90,P[0]);
	tran.Translate(-Edge,0);
	DrawObject();//中左
	tran.Translate(Edge,0);

	tran.Rotate(90,P[0]);
	DrawObject();//中中

	tran.Rotate(90,P[0]);
	tran.Translate(Edge,0);
	DrawObject();//中右
	tran.Translate(-Edge,0);

	tran.Rotate(90,P[0]);
	tran.Translate(-Edge,-Edge);
	DrawObject();//下左
	tran.Translate(Edge,Edge);

	tran.Rotate(90,P[0]);
	tran.Translate(0,-Edge);
	DrawObject();//下中
	tran.Translate(0,Edge);

	tran.Rotate(90,P[0]);
	tran.Translate(Edge,-Edge);
	DrawObject();//下右
	tran.Translate(-Edge,Edge);
}

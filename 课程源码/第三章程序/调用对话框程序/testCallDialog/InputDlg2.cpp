// InputDlg2.cpp : implementation file
//

#include "stdafx.h"
#include "testCallDialog.h"
#include "InputDlg2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInputDlg2 dialog


CInputDlg2::CInputDlg2(CWnd* pParent /*=NULL*/)
	: CDialog(CInputDlg2::IDD, pParent)
{
	//{{AFX_DATA_INIT(CInputDlg2)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	 length = 5.0;
	 width = 5.0;
}


void CInputDlg2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CInputDlg2)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP

	DDX_Text(pDX, IDC_EDIT1, width);
	DDV_MinMaxDouble(pDX, width, 0., 1000.);
    
	DDX_Text(pDX, IDC_EDIT2, length);
	DDV_MinMaxDouble(pDX, length, 0, 1000);

}


BEGIN_MESSAGE_MAP(CInputDlg2, CDialog)
	//{{AFX_MSG_MAP(CInputDlg2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInputDlg2 message handlers

void CInputDlg2::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();
}

#if !defined(AFX_INPUTDLG2_H__22BA531E_261C_457B_9940_B69D4D66D997__INCLUDED_)
#define AFX_INPUTDLG2_H__22BA531E_261C_457B_9940_B69D4D66D997__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputDlg2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInputDlg2 dialog

class CInputDlg2 : public CDialog
{
public:
	double	length;
	double	width;
// Construction
public:
	CInputDlg2(CWnd* pParent = NULL);   // standard constructor


// Dialog Data
	//{{AFX_DATA(CInputDlg2)
	enum { IDD = IDD_DIALOG2 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputDlg2)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInputDlg2)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTDLG2_H__22BA531E_261C_457B_9940_B69D4D66D997__INCLUDED_)

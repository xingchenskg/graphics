#if !defined(AFX_INPUTDLG3_H__1A9E2197_CDAA_4D2A_9266_E81EBD747815__INCLUDED_)
#define AFX_INPUTDLG3_H__1A9E2197_CDAA_4D2A_9266_E81EBD747815__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InputDlg3.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CInputDlg3 dialog

class CInputDlg3 : public CDialog
{
public:
	double  length;
	double	width;

// Construction
public:
	CInputDlg3(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CInputDlg3)
	enum { IDD = IDD_DIALOG3 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInputDlg3)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInputDlg3)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INPUTDLG3_H__1A9E2197_CDAA_4D2A_9266_E81EBD747815__INCLUDED_)

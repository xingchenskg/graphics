//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by testCallDialog.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TESTCATYPE                  129
#define IDD_DIALOG1                     131
#define IDD_DIALOG2                     132
#define IDD_DIALOG3                     136
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define ID_RECTANGLE                    32771

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1004
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

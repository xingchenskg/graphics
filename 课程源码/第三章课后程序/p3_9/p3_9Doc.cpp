// p3_9Doc.cpp : implementation of the CP3_9Doc class
//

#include "stdafx.h"
#include "p3_9.h"

#include "p3_9Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CP3_9Doc

IMPLEMENT_DYNCREATE(CP3_9Doc, CDocument)

BEGIN_MESSAGE_MAP(CP3_9Doc, CDocument)
	//{{AFX_MSG_MAP(CP3_9Doc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CP3_9Doc construction/destruction

CP3_9Doc::CP3_9Doc()
{
	// TODO: add one-time construction code here

}

CP3_9Doc::~CP3_9Doc()
{
}

BOOL CP3_9Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CP3_9Doc serialization

void CP3_9Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CP3_9Doc diagnostics

#ifdef _DEBUG
void CP3_9Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void CP3_9Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CP3_9Doc commands

// Line.h: interface for the CLine class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINE_H__E275816E_0156_444D_AE87_EF68251EE0F9__INCLUDED_)
#define AFX_LINE_H__E275816E_0156_444D_AE87_EF68251EE0F9__INCLUDED_
#include"P.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLine  
{
public:
	CLine();
	virtual ~CLine();
	void MoveTo(CDC *,CP);//移动到指定位置
	void MoveTo(CDC *,double,double,CRGB);
	void LineTo(CDC *,CP);//绘制直线，不含终点
	void LineTo(CDC *,double,double,CRGB);
	CRGB Interpolation(double,double,double,CRGB,CRGB);//线性插值
public:
	CP P0;//起点
	CP P1;//终点

};

#endif // !defined(AFX_LINE_H__E275816E_0156_444D_AE87_EF68251EE0F9__INCLUDED_)

// P.h: interface for the CP class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_P_H__431A2C5C_AA60_4494_A12D_B48990AE3A20__INCLUDED_)
#define AFX_P_H__431A2C5C_AA60_4494_A12D_B48990AE3A20__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include"RGB.h"
class CP  
{
public:
	CP();
	virtual ~CP();
	CP(double ,double);
	CP(double ,double ,CRGB);
	double x,y;
	CRGB c;

};

#endif // !defined(AFX_P_H__431A2C5C_AA60_4494_A12D_B48990AE3A20__INCLUDED_)
